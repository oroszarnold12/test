import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import deleteDialogReducer from '../features/dialogs/deleteDialogSlice';
import sideMenuReducer from '../features/dialogs/sideMenuSlice';
import reservationsReducer from '../features/reservations/reservationsSlice';

export const store = configureStore({
  reducer: {
    deleteDialog: deleteDialogReducer,
    sideMenu: sideMenuReducer,
    reservations: reservationsReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
