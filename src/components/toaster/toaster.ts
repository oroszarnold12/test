import { Position, Toaster } from '@blueprintjs/core';

// create a single instance to display toasts later
export const AppToaster = Toaster.create({
  position: Position.TOP_RIGHT,
});
