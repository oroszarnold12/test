import { Button, Classes, Dialog, Intent } from '@blueprintjs/core';
import { useSelector } from 'react-redux';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import {
  hideDeleteDialog,
  selectId,
  selectIsOpen,
} from '../features/dialogs/deleteDialogSlice';
import { deleteReservation } from '../features/reservations/reservationsSlice';
import { AppToaster } from './toaster/toaster';

export const ConfirmDeleteDialog: React.FC = () => {
  const isOpen = useAppSelector(selectIsOpen);
  const id = useSelector(selectId);
  const dispatch = useAppDispatch();

  const closeConfirmDialog = () => {
    dispatch(hideDeleteDialog());
  };

  const deleteClicked = () => {
    dispatch(deleteReservation(id));

    closeConfirmDialog();

    AppToaster.show({
      message: 'Reservation deleted!',
      intent: 'success',
      icon: 'trash',
    });
  };

  return (
    <Dialog
      icon="confirm"
      onClose={closeConfirmDialog}
      title="Confirm"
      isOpen={isOpen}
    >
      <div className={Classes.DIALOG_BODY}>
        <p>
          <strong>Are you sure you want to cancel this reservation?</strong>
        </p>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button onClick={closeConfirmDialog}>Close</Button>
          <Button onClick={deleteClicked} intent={Intent.SUCCESS}>
            Yes
          </Button>
        </div>
      </div>
    </Dialog>
  );
};
