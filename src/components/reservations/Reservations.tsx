import styles from './Reservations.module.css';
import { ReservationCard } from './ReservationCard';
import { useAppSelector } from '../../app/hooks';
import { selectReservations } from '../../features/reservations/reservationsSlice';
import { InputGroup } from '@blueprintjs/core';
import { useCallback, useEffect, useState } from 'react';
import { Reservation } from '../../model/reservation.model';

export const Reservations: React.FC = () => {
  const reservations = useAppSelector(selectReservations);
  const [filterValue, setFilterValue] = useState('');
  const [filteredReservations, setFilteredReservations] = useState<
    Reservation[]
  >([]);

  // filter the reservations by name, it's called when the value of the filter changes, or when the reservations array changes
  const filterReservations = useCallback(() => {
    setFilteredReservations(
      reservations.filter((reservation) => {
        return reservation.name
          .toLowerCase()
          .includes(filterValue.toLowerCase());
      })
    );
  }, [filterValue, reservations]);

  useEffect(() => {
    filterReservations();
  }, [filterReservations]);

  return (
    <div className={styles.reservations}>
      <div className={styles.filter}>
        <InputGroup
          asyncControl={true}
          leftIcon="filter"
          placeholder="Enter a name..."
          onChange={(e) => {
            setFilterValue(e.target.value);
          }}
        />
      </div>

      {filteredReservations.map((reservation) => {
        return (
          <ReservationCard key={reservation.id} reservation={reservation} />
        );
      })}
    </div>
  );
};
