import {
  Button,
  Card,
  Classes,
  FormGroup,
  H4,
  Icon,
  InputGroup,
  NumericInput,
} from '@blueprintjs/core';
import styles from './ReservationCreation.module.css';
import { DatePicker } from '@blueprintjs/datetime';
import { useEffect } from 'react';
import moment from 'moment';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  createReservation,
  removeReservationToUpdate,
  selectReservationToUpdate,
  updateReservation,
} from '../../features/reservations/reservationsSlice';
import { Reservation } from '../../model/reservation.model';
import { useHistory } from 'react-router-dom';
import { AppToaster } from '../toaster/toaster';

type Inputs = {
  name: string;
  date: Date;
  nrOfGuests: number;
  email: string;
};

export const ReservationCreation: React.FC = () => {
  const dispatch = useAppDispatch();
  const reservation = useAppSelector(selectReservationToUpdate);
  const history = useHistory();

  // used react-hook-form library to manage the form state
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm<Inputs>({ criteriaMode: 'all' });

  // if there is a reservation in Redux to update, we load the values into the form
  useEffect(() => {
    if (reservation) {
      reset({
        name: reservation.name,
        date: new Date(reservation.date),
        nrOfGuests: reservation.nrOfGuests,
        email: reservation.email,
      });
    }

    // clean the reservation from Redux
    return () => {
      dispatch(removeReservationToUpdate());
    };
  }, [reservation, dispatch, reset]);

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const newReseration: Reservation = {
      id: 0,
      name: data.name,
      date: data.date.toISOString(),
      nrOfGuests: data.nrOfGuests,
      email: data.email,
    };

    // if the reservation was present in Redux, we update it, else we create a new one
    if (reservation) {
      dispatch(
        updateReservation({
          id: reservation.id,
          reservation: newReseration,
        })
      );

      history.push('/');

      AppToaster.show({
        message: 'Reservation updated!',
        intent: 'success',
        icon: 'updated',
      });
    } else {
      dispatch(createReservation(newReseration));

      reset({
        name: '',
        date: undefined,
        nrOfGuests: 1,
        email: '',
      });

      AppToaster.show({
        message: 'Reservation created!',
        intent: 'success',
        icon: 'new-object',
      });
    }
  };

  return (
    <div className={styles.reservation_creation}>
      <Card className={styles.card} elevation={1}>
        <div className={styles.card_header}>
          <div className={styles.card_header_left}>
            <div className={styles.person_icon}>
              <Icon icon={reservation ? 'updated' : 'add'} color="white" />
            </div>

            <H4 className={styles.name}>
              {reservation ? 'Update Reservation' : 'Create Reservation'}
            </H4>
          </div>
        </div>

        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
          <FormGroup label="Name" labelFor="name-input">
            <Controller
              name="name"
              control={control}
              rules={{
                required: 'The name is required.',
                minLength: {
                  value: 5,
                  message: 'The name is too short',
                },
                maxLength: {
                  value: 30,
                  message: 'The name is too long.',
                },
              }}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <InputGroup
                  onChange={onChange}
                  value={value}
                  id="name-input"
                  placeholder="Enter your name..."
                  type="text"
                />
              )}
            />

            <ErrorMessage
              errors={errors}
              name="name"
              render={({ messages }) => {
                return messages
                  ? Object.entries(messages).map(([type, message]) => (
                      <div key={type} className={styles.error}>
                        <Icon icon="error" />
                        <p className={styles.error_message} key={type}>
                          {message}
                        </p>
                      </div>
                    ))
                  : null;
              }}
            />
          </FormGroup>

          <FormGroup className={styles.date_picker}>
            <Controller
              name="date"
              control={control}
              rules={{ required: 'The date is required' }}
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  onChange={onChange}
                  value={value}
                  className={Classes.ELEVATION_1}
                  timePickerProps={{
                    showArrowButtons: true,
                  }}
                />
              )}
            />
            <p className={styles.date_value}>
              Date:{' '}
              {watch('date')
                ? moment(watch('date')).format('YYYY MMM. Do - H:mm')
                : 'No date'}
            </p>

            <ErrorMessage
              errors={errors}
              name="date"
              render={({ messages }) => {
                return messages
                  ? Object.entries(messages).map(([type, message]) => (
                      <div key={type} className={styles.error}>
                        <Icon icon="error" />
                        <p className={styles.error_message} key={type}>
                          {message}
                        </p>
                      </div>
                    ))
                  : null;
              }}
            />
          </FormGroup>

          <FormGroup label="Nr. of guests" labelFor="nr-of-guests-input">
            <Controller
              name="nrOfGuests"
              control={control}
              rules={{
                required: 'The nr. of guests is required.',
                min: {
                  value: 1,
                  message: 'The nr. of guests should be atleast 1.',
                },
              }}
              defaultValue={1}
              render={({ field: { onChange, value } }) => (
                <NumericInput
                  onValueChange={onChange}
                  value={value}
                  id="nr-of-guests-input"
                  placeholder="Enter a number..."
                />
              )}
            />

            <ErrorMessage
              errors={errors}
              name="nrOfGuests"
              render={({ messages }) => {
                return messages
                  ? Object.entries(messages).map(([type, message]) => (
                      <div key={type} className={styles.error}>
                        <Icon icon="error" />
                        <p className={styles.error_message} key={type}>
                          {message}
                        </p>
                      </div>
                    ))
                  : null;
              }}
            />
          </FormGroup>

          <FormGroup
            label="Email"
            labelFor="email-input"
            labelInfo="(optional)"
          >
            <Controller
              name="email"
              control={control}
              rules={{
                pattern: {
                  value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                  message: 'The email is not valid.',
                },
              }}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <InputGroup
                  onChange={onChange}
                  value={value}
                  id="email-input"
                  placeholder="Enter your email address..."
                  type="email"
                />
              )}
            />

            <ErrorMessage
              errors={errors}
              name="email"
              render={({ messages }) => {
                return messages
                  ? Object.entries(messages).map(([type, message]) => (
                      <div key={type} className={styles.error}>
                        <Icon icon="error" />
                        <p className={styles.error_message} key={type}>
                          {message}
                        </p>
                      </div>
                    ))
                  : null;
              }}
            />
          </FormGroup>

          <Button intent={reservation ? 'primary' : 'success'} type="submit">
            {reservation ? 'Update' : 'Create'}
          </Button>
        </form>
      </Card>
    </div>
  );
};
