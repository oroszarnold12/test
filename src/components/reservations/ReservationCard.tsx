import { Button, Card, H4, Icon } from '@blueprintjs/core';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import { useAppDispatch } from '../../app/hooks';
import { showDeleteDialog } from '../../features/dialogs/deleteDialogSlice';
import { addReservationToUpdate } from '../../features/reservations/reservationsSlice';
import { Reservation } from '../../model/reservation.model';
import styles from './ReservationCard.module.css';

interface PropsType {
  reservation: Reservation;
}

export const ReservationCard: React.FC<PropsType> = ({
  reservation: { id, name, date, nrOfGuests, email },
}) => {
  const history = useHistory();
  const dispatch = useAppDispatch();

  // before going to the creation page, we dispatch to Redux the reservation that will be updated
  const routeToCreationPageToUpdate = () => {
    dispatch(
      addReservationToUpdate({
        id: id,
        name: name,
        date: date,
        nrOfGuests: nrOfGuests,
        email: email,
      })
    );

    history.push({
      pathname: '/create',
    });
  };

  return (
    <Card className={styles.card} elevation={1}>
      <div className={styles.card_header}>
        <div className={styles.card_header_left}>
          <div className={styles.person_icon}>
            <Icon icon="person" color="white" />
          </div>

          <H4 className={styles.name}>{name}</H4>
        </div>

        <Button
          className={styles.edit_button}
          icon="edit"
          minimal={true}
          onClick={routeToCreationPageToUpdate}
        />
      </div>

      <div className={styles.content}>
        <div className={styles.row}>
          <div className={styles.label}>Date:</div>

          <div>{moment(date).format('YYYY MMM. Do - H:mm')}</div>
        </div>

        <div className={styles.row}>
          <div className={styles.label}>Nr. of guests:</div>

          <div>{nrOfGuests}</div>
        </div>

        {email && (
          <div className={styles.row}>
            <div className={styles.label}>Email:</div>

            <div className={styles.email}>{email}</div>
          </div>
        )}

        <Button
          className={styles.delete_button}
          icon="trash"
          intent="danger"
          minimal={true}
          onClick={() => dispatch(showDeleteDialog(id))}
        />
      </div>
    </Card>
  );
};
