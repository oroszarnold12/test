import { Button } from '@blueprintjs/core';
import { useAppDispatch } from '../../app/hooks';
import { showSideMenu } from '../../features/dialogs/sideMenuSlice';
import styles from './Header.module.css';

export const Header: React.FC = () => {
  const dispatch = useAppDispatch();

  const menuClicked = () => {
    dispatch(showSideMenu());
  };

  return (
    <header className={styles.header}>
      <div>Reservations</div>

      <Button
        className={styles.menu_button}
        icon="menu"
        minimal={true}
        onClick={menuClicked}
      />
    </header>
  );
};
