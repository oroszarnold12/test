import { Icon, IconName } from '@blueprintjs/core';
import styles from './SideMenuItem.module.css';

interface PropsType {
  icon: IconName;
  text: string;
  onClick: () => void;
}

export const SideMenuItem: React.FC<PropsType> = ({ icon, text, onClick }) => {
  return (
    <div className={styles.side_menu_item} onClick={onClick}>
      <Icon icon={icon} color="white" />
      <div className={styles.text}>{text}</div>
    </div>
  );
};
