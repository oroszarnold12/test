import { Icon } from '@blueprintjs/core';
import { useHistory } from 'react-router-dom';
import { useAppDispatch } from '../../app/hooks';
import { hideSideMenu } from '../../features/dialogs/sideMenuSlice';
import { SideMenuItem } from './SideMenuItem';
import styles from './SideMenuContent.module.css';

export const SideMenuContent: React.FC = () => {
  const history = useHistory();
  const dispatch = useAppDispatch();

  const routeToCreationPage = () => {
    history.push('/create');
    dispatch(hideSideMenu());
  };

  const routeToHomePage = () => {
    history.push('/');
    dispatch(hideSideMenu());
  };

  return (
    <div className={styles.background}>
      <div className={styles.side_menu}>
        <div className={styles.side_menu_header}>
          <Icon className={styles.menu_icon} icon="menu" color="white"></Icon>
          Menu
        </div>
        <SideMenuItem icon="home" text="Home" onClick={routeToHomePage} />
        <SideMenuItem
          icon="plus"
          text="Create Reservation"
          onClick={routeToCreationPage}
        />
      </div>
    </div>
  );
};
