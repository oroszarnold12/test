import { Drawer } from '@blueprintjs/core';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  hideSideMenu,
  selectIsOpen,
} from '../../features/dialogs/sideMenuSlice';
import { SideMenuContent } from './SideMenuContent';

export const SideMenu: React.FC = () => {
  const dispatch = useAppDispatch();
  const isOpen = useAppSelector(selectIsOpen);

  const handleClose = () => {
    dispatch(hideSideMenu());
  };

  return (
    <Drawer onClose={handleClose} isOpen={isOpen} position="right" size="260px">
      <SideMenuContent />
    </Drawer>
  );
};
