import React from 'react';
import './App.css';
import { ConfirmDeleteDialog } from './components/ConfirmDeleteDialog';
import { Header } from './components/header/Header';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { SideMenu } from './components/side-menu/SideMenu';
import { Reservations } from './components/reservations/Reservations';
import { ReservationCreation } from './components/reservations/ReservationCreation';
import { SideMenuContent } from './components/side-menu/SideMenuContent';

function App() {
  return (
    <div className="app">
      <Router>
        <div className="sidebar">
          <SideMenuContent />
        </div>

        <div className="main">
          <Header />

          <Switch>
            <Route path="/create">
              <ReservationCreation />
            </Route>
            <Route path="/">
              <Reservations />
            </Route>
          </Switch>

          <SideMenu />

          <ConfirmDeleteDialog />
        </div>
      </Router>
    </div>
  );
}

export default App;
