export interface Reservation {
  id: number;
  name: string;
  date: string;
  nrOfGuests: number;
  email?: string;
}
