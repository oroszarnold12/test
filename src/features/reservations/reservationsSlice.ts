import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { Reservation } from '../../model/reservation.model';

// This slice manages the reservations in the application. In the real world scenario this data would come from a backend.
// This slice also manages the state that tells the creation page, that it is updating or creationg a new reservation.

export interface ReservationsState {
  reservations: Reservation[];
  lastId: number;
  reservationToUpdate?: Reservation;
}

const initialState: ReservationsState = {
  reservations: [
    {
      id: 1,
      name: 'Orosz Arnold',
      date: '2021-08-06T09:10:00.158Z',
      nrOfGuests: 2,
      email: 'oroszarnold12@yahoo.com',
    },
    {
      id: 2,
      name: 'Nagy Adam',
      date: '2021-08-05T10:00:00.158Z',
      nrOfGuests: 4,
      email: 'nagyadam@yahoo.com',
    },
    {
      id: 3,
      name: 'Kiss Eniko',
      date: '2021-08-12T17:00:00.158Z',
      nrOfGuests: 2,
    },
    {
      id: 4,
      name: 'Kovacs Agnes',
      date: '2021-08-15T11:30:00.158Z',
      nrOfGuests: 2,
      email: 'kovacsagnes@yahoo.com',
    },
    {
      id: 5,
      name: 'Gal Eva',
      date: '2021-08-19T12:30:00.158Z',
      nrOfGuests: 2,
      email: 'galeva@yahoo.com',
    },
    {
      id: 6,
      name: 'Loga Marton',
      date: '2021-08-25T14:40:00.158Z',
      nrOfGuests: 2,
      email: 'logamarton@yahoo.com',
    },
  ],
  lastId: 6,
};

export const reservationsSlice = createSlice({
  name: 'reservations',
  initialState,
  reducers: {
    createReservation: (state, action: PayloadAction<Reservation>) => {
      action.payload.id = ++state.lastId;
      state.reservations.push(action.payload);
    },
    updateReservation: (
      state,
      action: PayloadAction<{ id: Number; reservation: Reservation }>
    ) => {
      state.reservations.forEach((reservation) => {
        if (reservation.id === action.payload.id) {
          const updatedReservation = action.payload.reservation;

          reservation.name = updatedReservation.name;
          reservation.date = updatedReservation.date;
          reservation.nrOfGuests = updatedReservation.nrOfGuests;
          reservation.email = updatedReservation.email;
        }
      });
    },
    deleteReservation: (state, action: PayloadAction<number | null>) => {
      state.reservations = state.reservations.filter((reservation) => {
        return reservation.id !== action.payload;
      });
    },
    addReservationToUpdate: (state, action: PayloadAction<Reservation>) => {
      state.reservationToUpdate = action.payload;
    },
    removeReservationToUpdate: (state) => {
      state.reservationToUpdate = undefined;
    },
  },
});

export const {
  createReservation,
  updateReservation,
  deleteReservation,
  addReservationToUpdate,
  removeReservationToUpdate,
} = reservationsSlice.actions;

export const selectReservations = (state: RootState) =>
  state.reservations.reservations;

export const selectReservationToUpdate = (state: RootState) =>
  state.reservations.reservationToUpdate;

export default reservationsSlice.reducer;
