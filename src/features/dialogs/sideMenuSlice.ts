import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface SideMenuState {
  isOpen: boolean;
}

const initialState: SideMenuState = {
  isOpen: false,
};

export const sideMenuSlice = createSlice({
  name: 'sideMenu',
  initialState,
  reducers: {
    showSideMenu: (state) => {
      state.isOpen = true;
    },

    hideSideMenu: (state) => {
      state.isOpen = false;
    },
  },
});

export const { showSideMenu, hideSideMenu } = sideMenuSlice.actions;

export const selectIsOpen = (state: RootState) => state.sideMenu.isOpen;

export default sideMenuSlice.reducer;
