import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

// This slice manages the confirm dialog state for delete operation

export interface DeleteDialogState {
  isOpen: boolean;
  id: number | null;
}

const initialState: DeleteDialogState = {
  isOpen: false,
  id: null,
};

export const deleteDialogSlice = createSlice({
  name: 'deleteDialog',
  initialState,
  reducers: {
    showDeleteDialog: (state, action: PayloadAction<number>) => {
      state.isOpen = true;
      state.id = action.payload;
    },

    hideDeleteDialog: (state) => {
      state.isOpen = false;
      state.id = null;
    },
  },
});

export const { showDeleteDialog, hideDeleteDialog } = deleteDialogSlice.actions;

export const selectIsOpen = (state: RootState) => state.deleteDialog.isOpen;
export const selectId = (state: RootState) => state.deleteDialog.id;

export default deleteDialogSlice.reducer;
